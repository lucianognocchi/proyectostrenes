﻿using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerAnchos
{
    public class ObtenerAnchosModel : GenericModel
    {
        public IList<ObtenerAnchosItem> Anchos { get; set; }

        public ObtenerAnchosModel()
        {
            Anchos = new List<ObtenerAnchosItem>();
        }

    }
}
