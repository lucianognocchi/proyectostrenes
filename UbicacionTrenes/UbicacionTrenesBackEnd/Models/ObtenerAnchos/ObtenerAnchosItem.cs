﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerAnchos
{
    public class ObtenerAnchosItem
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

    }
}
