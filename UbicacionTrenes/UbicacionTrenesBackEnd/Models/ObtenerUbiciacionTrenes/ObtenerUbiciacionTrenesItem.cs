﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerUbiciacionTrenes
{
    public class ObtenerUbiciacionTrenesItem
    {
        public string Transporte { get; set; }
        public int Gondolas { get; set; }
        public string Toneladas { get; set; }
        public string Planta { get; set; }
        public string Coordenada { get; set; }
    }
}
