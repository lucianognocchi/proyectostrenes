﻿using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerUbiciacionTrenes
{
    public class ObtenerUbiciacionTrenesModel : GenericModel
    {
        public IList<ObtenerUbiciacionTrenesItem> Ubicaciones { get; set; }

        public ObtenerUbiciacionTrenesModel()
        {
            Ubicaciones = new List<ObtenerUbiciacionTrenesItem>();
        }

    }
}
