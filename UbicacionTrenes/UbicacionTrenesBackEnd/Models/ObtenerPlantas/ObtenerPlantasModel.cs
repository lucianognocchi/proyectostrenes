﻿using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerPlantas
{
    public class ObtenerPlantasModel : GenericModel
    {
        public IList<ObtenerPlantasItem> Plantas { get; set; }

        public ObtenerPlantasModel()
        {
            Plantas = new List<ObtenerPlantasItem>();
        }

    }
}
