﻿using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerTipoMateriales
{
    public class ObtenerTipoMaterialesModel : GenericModel
    {
        public IList<ObtenerTipoMaterialesItem> TipoMateriales { get; set; }

        public ObtenerTipoMaterialesModel()
        {
            TipoMateriales = new List<ObtenerTipoMaterialesItem>();
        }

    }
}
