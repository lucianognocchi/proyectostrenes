﻿using Common.Models.Comunes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerGrados
{
    public class ObtenerGradosImportModel : ImportModel
    {
        public string texto { get; set; }
    }
}
