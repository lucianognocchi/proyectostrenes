﻿using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerGrados
{
    public class ObtenerGradosModel : GenericModel
    {
        public IList<ObtenerGradosItem> Grados { get; set; }

        public ObtenerGradosModel()
        {
            Grados = new List<ObtenerGradosItem>();
        }

    }
}
