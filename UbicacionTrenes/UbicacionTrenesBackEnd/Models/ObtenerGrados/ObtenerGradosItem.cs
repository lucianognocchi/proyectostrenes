﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.Models.ObtenerGrados
{
    public class ObtenerGradosItem
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

    }
}
