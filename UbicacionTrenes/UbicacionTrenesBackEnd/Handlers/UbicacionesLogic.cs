﻿using Common.Models.Comunes;

using Common.HelperLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UbicacionTrenesBackEnd.DataAccess;
using Common.Models.Generic;
using UbicacionTrenesBackEnd.Models.ObtenerUbiciacionTrenes;
using UbicacionTrenesBackEnd.Models.ObtenerPlantas;
using UbicacionTrenesBackEnd.Models.ObtenerAnchos;
using UbicacionTrenesBackEnd.Models.ObtenerGrados;
using UbicacionTrenesBackEnd.Models.ObtenerTipoMateriales;

namespace UbicacionTrenesBackEnd.Handlers
{
    public class UbicacionesLogic : HelperLogic
    {
        private DataAccess.UbicacionesDataAccess _ubicacionesDataAccess = new DataAccess.UbicacionesDataAccess();

        public GenericModel ObtenerAnchos(ObtenerAnchosImportModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", MethodBase.GetCurrentMethod().DeclaringType.Name,
    '.', MethodBase.GetCurrentMethod().Name);

            var result = new ObtenerAnchosModel();


            var ds = _ubicacionesDataAccess.ObtenerAnchos(model);

            return result;
        }
        public GenericModel ObtenerGrados(ObtenerGradosImportModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", MethodBase.GetCurrentMethod().DeclaringType.Name,
    '.', MethodBase.GetCurrentMethod().Name);

            var result = new ObtenerGradosModel();
            var ds = _ubicacionesDataAccess.ObtenerGrados(model);

            result.Grados = GetList<ObtenerGradosItem>(ds.Tables["Table"]);
            return result;
        }
        public GenericModel ObtenerPlantas(ObtenerPlantasImportModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                '.', MethodBase.GetCurrentMethod().Name);

            var result = new ObtenerPlantasModel();
            var ds = _ubicacionesDataAccess.ObtenerPlantas(model);

            result.Plantas = GetList<ObtenerPlantasItem>(ds.Tables["Table"]);
            return result;

        }
        public GenericModel ObtenerTipoMateriales(ObtenerTipoMaterialesImportModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", MethodBase.GetCurrentMethod().DeclaringType.Name,
   '.', MethodBase.GetCurrentMethod().Name);

            var result = new ObtenerTipoMaterialesModel();
            var ds = _ubicacionesDataAccess.ObtenerTipoMateriales(model);
            result.TipoMateriales = GetList<ObtenerTipoMaterialesItem>(ds.Tables["Table"]);
            return result;
        }
        public GenericModel ObtenerUbiciacionTrenes(ObtenerUbiciacionTrenesImportModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", MethodBase.GetCurrentMethod().DeclaringType.Name,
               '.', MethodBase.GetCurrentMethod().Name);
            var result = new ObtenerUbiciacionTrenesModel();
            var ds = _ubicacionesDataAccess.ObtenerUbicacionTrenes(model);
            result.Ubicaciones = GetList<ObtenerUbiciacionTrenesItem>(ds.Tables["Table"]);
            return result;

        }

    }
}
