﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbicacionTrenesBackEnd.DataAccess
{
    public static class Queries
    {
        public static string obtenerUbiciacionTrenes => "PRC_UbicacionTren"; // "PRC_TP_UBICACION_TRENES_LM";
        public static string obtenerUbiciacionCamiones => "PRC_TP_UBICACION_CAMIONES_LM";
        public static string obtenerUbiciacionBarcos => "PRC_TP_UBICACION_BARCOS_LM";
        public static string obtenerUbiciacionAncho => "PRC_TP_UBICACION_ANCHO_LM";
        public static string obtenerUbiciacionGrado => "PRC_TP_UBICACION_GRADO_LM";
        public static string obtenerUbiciacionPlanta => "PRC_TP_UBICACION_PLANTA_LM";
        public static string obtenerUbiciacionTipoMaterial => "PRC_TP_UBICACION_TIPOMATERIAL_LM";
    }
}
