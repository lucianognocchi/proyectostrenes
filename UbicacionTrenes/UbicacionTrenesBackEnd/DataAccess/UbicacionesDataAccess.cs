﻿using Common.DataAccess;
using Common.Models.Comunes;  
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UbicacionTrenesBackEnd.Models.ObtenerAnchos;
using UbicacionTrenesBackEnd.Models.ObtenerGrados;
using UbicacionTrenesBackEnd.Models.ObtenerPlantas;
using UbicacionTrenesBackEnd.Models.ObtenerTipoMateriales;
using UbicacionTrenesBackEnd.Models.ObtenerUbiciacionTrenes;

namespace UbicacionTrenesBackEnd.DataAccess
{
    public class UbicacionesDataAccess : Connection
    {
        public UbicacionesDataAccess() { }
    
        public DataSet ObtenerPlantas(ObtenerPlantasImportModel model)
        {
            dataSet = new DataSet();
            
            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(Queries.obtenerUbiciacionPlanta, connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.Fill(dataSet);
            }

            return dataSet;
        }
        public DataSet ObtenerAnchos(ObtenerAnchosImportModel model)
        {
            dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(Queries.obtenerUbiciacionAncho, connection);
                adapter.SelectCommand.Parameters.Add("@Texto", SqlDbType.NVarChar).Value = "";
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.Fill(dataSet);
            }

            return dataSet;
        }
        public DataSet ObtenerGrados(ObtenerGradosImportModel model)
        {
            dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(Queries.obtenerUbiciacionGrado, connection);
                adapter.SelectCommand.Parameters.Add("@Texto", SqlDbType.NVarChar).Value = "";
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.Fill(dataSet);
            }

            return dataSet;
        }
        
        public DataSet ObtenerTipoMateriales(ObtenerTipoMaterialesImportModel model)
        {
            dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(Queries.obtenerUbiciacionTipoMaterial, connection);
                //adapter.SelectCommand.Parameters.Add("@Texto", SqlDbType.NVarChar).Value = "";
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.Fill(dataSet);
            }

            return dataSet;
        }
        public DataSet ObtenerUbicacionTrenes(ObtenerUbiciacionTrenesImportModel model)
        {
            dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(Queries.obtenerUbiciacionTrenes, connection);
                //adapter.SelectCommand.Parameters.Add("@Texto", SqlDbType.NVarChar).Value = "";
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.Fill(dataSet);
            }

            return dataSet;
        }



    }
}
