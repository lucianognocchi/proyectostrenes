﻿function initMap() {
    var uluru = { lat: 22.486468, lng: -97.919577};

    var map = new google.maps.Map(
        document.getElementById('MapaPrincipal'),
        { zoom: 7, center: { lat: 24.5939172, lng: -98.5233576} }
    );

    marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "Images/train.png"
    });  
    var marker_1 = new google.maps.Marker({
        position: { lat: 25.833401, lng: -97.532205 },
        map: map,
        type: 'info',
        icon: "Images/train.png"
    });

    var contentString ='<div class="info-marker"><div class="info-bar"></div> <div class="info-content">      <label>UBICACION</label>        <div><label>Tren:</label><span>FSR</span></div>            <div><label>No. Gondolas:</label><span>57</span></div>            <div><label>Toneladas:</label><span>72,049</span></div>            <div><label>Plantas:</label><span>Churubusco</span></div>        </div>    </div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    var contentString_1 = '<div class="info-marker">'
       +' <div class="info-bar"></div>'
        +'  <div class="info-content">'
           +' <label>UBICACION</label>'
           +' <div><label>Tren:</label><span>FSR T</span></div>'
           +' <div><label>No. Gondolas:</label><span>180</span></div>'
           +' <div><label>Toneladas:</label><span>15,049</span></div>'
        +' <div><label>Plantas:</label><span>Churubusco</span></div>'
        +' </div>'
        +' </div>';

    var infowindow_1 = new google.maps.InfoWindow({
        content: contentString_1
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    marker_1.addListener('click', function () {
        infowindow_1.open(map, marker_1);
    });
    
    marker.addListener('dblclick', function () {
        Home.mostrarOcultar(true);
        $("#planta").text("CHURUBUSCO");
        $("#packinglist").text("CHU BONISLAND OCT18");
        $("#piezas").text("72,049");
        $("#toneladas").text("3,614");
        $("#proveedor").text("TERNIUM BRASIL");
        $("#tren").text("FSR");
        $("#fechaSalida").text("08-01-2019 ");
        $("#status").text("EN VIAJE");
        $("#ubicacion").text("MONTERREY");
                                
    });
    marker_1.addListener('dblclick', function () {
        Home.mostrarOcultar(true);
        $("#planta").text("CHURUBUSCO 2 ");
        $("#packinglist").text("CHU BONISLAND OCT18 2");
        $("#piezas").text("15,049");
        $("#toneladas").text("3,614");
        $("#proveedor").text("TERNIUM Mexico");
        $("#tren").text("FSR");
        $("#fechaSalida").text("08-10-2019 ");
        $("#status").text("En Reparación");
        $("#ubicacion").text("MONTERREY");

    });
    
}