﻿; (function ($) {
    $.fn.gridResponsable = function (method) {

        // plugin's default options
        var defaults = {}

        // this will hold the merged default and user-provided properties
        // you will have to access the plugin's properties through this object!
        // settings.propertyName
        var settings = {
        }

        // public methods can be called as
        // $(selector).gridAtributos('methodName', arg1, arg2, ... argn)
        var methods = {

            // this the constructor method that gets called when
            // the object is created
            init: function (options) {

                // iterate through all the DOM elements we are
                // attaching the plugin to
                return this.each(function () {

                    settings = $.extend({}, defaults, options);
                    $(this).data("settings", settings);
                    $(this).data("data", settings.data);

                    // Toma el id del elemento como identificador principal
                    var idTiles = $(this).attr("id");

                    $(this).addClass("pg-content");

                    // Arma los elementos principales de la grilla
                    var gridResponsable = "<table id='container" + idTiles + "' class='display table responsive' style='width:100%;' data-effect='bounceInLeft' >"
                        + "</table>";

                    $(this).html(gridResponsable);
                });
            },

            redraw: function () {
                var idTiles = $(this).attr("id");
                var data = $("#" + idTiles).data("data");
                helpers.redraw(idTiles, data);

                // Anima la visualización
                $("#container" + idTiles).animatePanel();
            },

            getSelectedItem: function () {
                var idTiles = $(this).attr("id");
                return $("#container" + idTiles).DataTable().rows($('.gridResponsableItem:checked').parents('td')).data().toArray();
            }

        }

        // private methods
        // private methods can be called as
        // helpers.methodName(arg1, arg2, ... argn)
        var helpers = {
            redraw: function (idTiles, data) {
                $("#container" + idTiles).DataTable({
                    paging: false, info: false, scrollY: '36vh', scrollX: true, language: tableLang, order: [], destroy: true,
                    data: data,
                    columns: [
                        {
                            data: null, className: 'textoizquierdacabecera', title:  'PROYECTO' , order: true, render: function (data, type, row, meta) {
                                return '<spam  style="float: right;padding-right: 14px;;" class="colorAzul text-right"><strong>' + data.PROYECTO +'</strong> </spam>';
                            }
                        },

                        { data: 'CLIENTE', title:  'CLIENTE'   },
                        { data: 'NORMA', title:  'NORMA'   }, 
                       
                    ]
                
                     
             
                });
                //helpers.addEvents();
                $("#containergridResponsable_wrapper > div:nth-child(2)").prepend('<div class="col-sm-6"></div><div class="col-sm-6" style="padding-top: 10px;"><spam style="float: left;text-align: center;width: 61%;border-bottom: 1px solid #ddd !important;margin-left: -4%;"><strong>RESPONSABLE</strong></spam></div>');

                $("#containergridResponsable_filter").prepend('<button style="margin-right: 10px;" class="btn btn-primary" data-href="' + window.location.origin + "/Producto/PPAP" + (UtilJs.redirect != "" ? '?redirect=' + UtilJs.redirect : '') + '" onclick="UtilJs.handleUrlApp(this)"'  /*+ (Proyecto.consulta ? 'disabled' : ''*/  + '>PPAP</button>');

            },

            //addEvents: function () {
            //    UtilJs.addCommonListener({
            //        selector: '.gridResponsableItem',
            //        callback: function (event) {
            //            var prevState = $(this).prop('checked');
            //            $('.gridResponsableItem').prop('checked', false);
            //            $(this).prop('checked', prevState);
            //        }
            //    });
            //}
        }

        // if a method as the given argument exists
        if (methods[method]) {

            // call the respective method
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));

            // if an object is given as method OR nothing is given as argument
        } else if (typeof method === "object" || !method) {

            // call the initialization method
            return methods.init.apply(this, arguments);

            // otherwise
        } else {

            // trigger an error
            $.error('Method "' + method + '" does not exist in gridResponsable plugin!');
        }
    }
})(jQuery);