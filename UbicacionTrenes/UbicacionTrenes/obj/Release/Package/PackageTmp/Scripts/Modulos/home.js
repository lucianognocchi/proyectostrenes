﻿var i18n = i18n  || {
    get: function (text) {
        return text;}

}
var Home = Home ||
    {

        init: function (model) {
            //Call Js Error Handler module if defined.

            //TODO implement rest of init logic
            Home.addEventosIniciales();
            UtilJs.pluginConfig("filtrosPlanta", "gridFiltroPlantas", filtrosPlantas);
            Home.mostrarOcultar(false);
        },
        addEventosIniciales: function () {
            UtilJs.addCommonListener({
                selector: '#btnLimpiarLevel0',
                callback: function (event) {
                    //todo event limpiar
                }
            });
            //UtilJs.addCommonListener({
            //    selector: '#MapaPrincipal',
            //    callback: function (event) {

            //        if($("#info-panel")[0].style.width == "350px")
            //        Home.mostrarOcultar(false);

            //    }
            //});

            UtilJs.addCommonListener({
                selector: '#BtnBuscar',
                callback: function (event) {
                    Home.Buscar();
                }
            });


        },



        buscar: function () {
            UtilJs.pluginConfig("filtrosPlanta", "gridTipoProceso", materiales);
        },

        mostrarOcultar: function (val) {


            if (!val) {
                $("#info-panel")[0].style.width = '0px';
                $("#MapaPrincipal")[0].style.width = '100%';
            } else {

                $("#info-panel")[0].style.width = '350px';
                $("#MapaPrincipal")[0].style.width = 'calc(100% - 350px)';
            }

        }
    }