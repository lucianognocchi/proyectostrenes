﻿using UbicacionTrenes.Helper;
using System.Web.Mvc;
using UbicacionTrenes.Models;
using UbicacionTrenesBackEnd.Models.ObtenerPlantas;
using UbicacionTrenesBackEnd.Handlers;
using UbicacionTrenesBackEnd.Models.ObtenerUbiciacionTrenes;
using UbicacionTrenesBackEnd.Models.ObtenerTipoMateriales;
using UbicacionTrenesBackEnd.Models.ObtenerGrados;
using UbicacionTrenesBackEnd.Models.ObtenerAnchos;

namespace UbicacionTrenes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ObjetenerPuntosTrenes(ObtenerUbiciacionTrenesImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerUbiciacionTrenes(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public JsonResult ObtenerPuntosMapa(ObtenerPuntosMapaModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);

            var response = new[]
                              {
                            new {  lat = 22.486468,  lng =-97.919577,   },
                            new {  lat = 22.586468,        lng =-97.919577 },
                            new {  lat =22.686468,         lng = -97.919577 },
                            new {  lat = 22.786468,           lng = -97.919577 },
                            new {  lat = 25.833401,        lng =-97.532205 }
                        };
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public ActionResult VistaBarco()
        {
            return View();
        }
        public JsonResult ObtenerPuntosCamionMapa(ObtenerPuntosMapaModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);

            var response = new[] {
                new {lat = 22.774155, lng = -100.504072},
                new {lat = 21.653344, lng = -98.924615},
                new {lat = 20.299296, lng = -98.485355},
                new {lat = 19.679832, lng = -100.066690
                }
            };
            
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult VistaCamion()
        {
            return View();
        }
        public JsonResult ObtenerPuntosBarcoMapa(ObtenerPuntosMapaModel model)
        {
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);

            var response = new[] {
                new {lat = 12.537404, lng = -64.185008},
                new {lat = 8.215839, lng = -53.115667},
                new {lat = 3.495875, lng = -40.992103},
                new {lat = -3.706398, lng = -30.098465},
                new {lat = -18.162556, lng = -37.126618},
                new {lat = -38.148382, lng = -56.278336}
            };
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public JsonResult ObtenerAnchos(ObtenerAnchosImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerAnchos(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public JsonResult ObtenerGrados(ObtenerGradosImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerGrados(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public JsonResult ObtenerPlantas(ObtenerPlantasImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerPlantas(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public JsonResult ObtenerTipoMateriales(ObtenerTipoMaterialesImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerTipoMateriales(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public JsonResult ObtenerUbiciacionTrenes(ObtenerUbiciacionTrenesImportModel model)
        {
            UbicacionesLogic logica = new UbicacionesLogic();
            var currentClassMethod = string.Format("{0}{1}{2}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, '.', System.Reflection.MethodBase.GetCurrentMethod().Name);
            var response = GenericMethods.PerformFunction(currentClassMethod, () => logica.ObtenerUbiciacionTrenes(model));
            return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

    }
}