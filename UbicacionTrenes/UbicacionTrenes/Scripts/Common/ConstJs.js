﻿//Fichero diseñado para incluir las constantes que utilizamos en el proyecto. La idea es incluir todas las otras constantes que utilicemos.

var httpMethod = {
    Post: 'POST',
    Get: 'GET'
};
var timing = {
    Moving: 200,
    EllapseDefault: 480,
    EllapseIE: 800
};
var defaultStr = {
    YesSp: 'S',
    No: 'N',
    Timeout: 'timeout'
};
var color = {
    Primary: '#34495e'
};
var transitions = {
    "transition": "transitionend",
    "OTransition": "oTransitionEnd",
    "MozTransition": "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
};

var filtrosPlantas = [
    {
        id: '1', Nombre: 'Churubusco'
    },
    {
        id: '2', Nombre: 'Guerrero'
    }, {
        id: '3', Nombre: 'Pesqueria'
    }
    , {
        id: '4', Nombre: 'Universidad'
    }
    , {
        id: '5', Nombre: 'Bodega Externa'
    }
    , {
        id: '6', Nombre: 'Sin Asignar'
    }
];
 
var tableLang = {
    "zeroRecords": "No se encontraron registros",
    "infoEmpty": "No se encontraron registros",
    "search": "BUSCAR",
    "paginate": {
        "sFirst": "Primera", // This is the link to the first page
        "sPrevious": "Anterior", // This is the link to the previous page
        "sNext": "Siguiente", // This is the link to the next page
        "sLast": "Última"// This is the link to the last page
    }
};