﻿var i18n = i18n  || {
    get: function (text) {
        return text;}

}
var Home = Home ||
{
    markers: [],

    init: function (model) {
        //Call Js Error Handler module if defined.

        //TODO implement rest of init logic
        Home.addEventosIniciales();
        UtilJs.pluginConfig("filtrosPlanta", "gridFiltroPlantas", filtrosPlantas);
        Home.mostrarOcultar(false);
    },
    addEventosIniciales: function () {
        UtilJs.addCommonListener({
            selector: '#btnLimpiarLevel0',
            callback: function (event) {
                //todo event limpiar
            }
        });
        //UtilJs.addCommonListener({
        //    selector: '#MapaPrincipal',
        //    callback: function (event) {

        //        if($("#info-panel")[0].style.width == "350px")
        //        Home.mostrarOcultar(false);
        //
        //    }
        //});

        UtilJs.addCommonListener({
            selector: '#BtnBuscar',
            callback: function (event) {
                Home.Buscar();
            }
        });
    },

    Buscar: function () {
        $.ajax({
            url: SitePath + "/Home/ObtenerPuntosBarcoMapa",
            data: {},
            success: function (result) {

                for (var i = 0; i < Home.markers.length; i++) {
                    Home.markers[i].setMap(null);
                }

                Home.markers = [];

                for (var i = 0; i < result.length; i++) {
                    var info = result[i];
                    
                    var marker = new google.maps.Marker({
                        position: { lat: info.lat, lng: info.lng},
                        map: map,
                        icon: "/Images/barco.png"
                    });

                    var contentString = '<div class="info-marker">'
                        + ' <div class="info-bar"></div>'
                        + '  <div class="info-content">'
                        + ' <label>UBICACION</label>'
                        + ' <div><label>Barco:</label><span>FSR T</span></div>'
                        + ' <div><label>No. Gondolas:</label><span>180</span></div>'
                        + ' <div><label>Toneladas:</label><span>15,049</span></div>'
                        + ' <div><label>Plantas:</label><span>Churubusco</span></div>'
                        + ' </div>'
                        + ' </div>';


                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    marker.addListener('click', function () {
                        infowindow.open(map, this);
                    });
                    
                    marker.addListener('dblclick', function () {
                        Home.mostrarOcultar(true);
                        $("#planta").text("CHURUBUSCO");
                        $("#packinglist").text("CHU BONISLAND OCT18");
                        $("#piezas").text("72,049");
                        $("#toneladas").text("3,614");
                        $("#proveedor").text("TERNIUM BRASIL");
                        $("#tren").text("FSR");
                        $("#fechaSalida").text("08-01-2019 ");
                        $("#status").text("EN VIAJE");
                        $("#ubicacion").text("MONTERREY");

                    });

                    Home.markers.push(marker);
                }
            }
        });
    },

    mostrarOcultar: function (val) {

            if (!val) {
                $("#info-panel")[0].style.display = 'none';
                $("#info-panel")[0].style.width = '0px';
                $("#MapaPrincipal")[0].style.width = '100%';
            } else {
                $("#info-panel")[0].style.display = 'block';
                $("#info-panel")[0].style.width = '350px';
                $("#MapaPrincipal")[0].style.width = 'calc(100% - 350px)';
            }

        }
    }