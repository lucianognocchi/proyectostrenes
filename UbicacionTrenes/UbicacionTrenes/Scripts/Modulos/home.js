﻿var i18n = i18n  || {
    get: function (text) {
        return text;}

}
var Home = Home ||
{
    markers: [],
    plantas: {},

    init: function (model) {
        //Call Js Error Handler module if defined.

        //TODO implement rest of init logic
        Home.addEventosIniciales();
        //UtilJs.pluginConfig("filtrosPlanta", "gridFiltroPlantas", filtrosPlantas);
        Home.mostrarOcultar(false);
        Home.fillComboPlantas();
        $("#select_Ancho").select2({ placeholder: "SELECCIONE"});
        $("#select_Grado").select2({ placeholder: "SELECCIONE" });
        $("#select_Material").select2({ placeholder: "SELECCIONE" });
    },

    fillComboMateriales: function () {
        UtilJs.execAjaxTrace({
            url: SitePath + "Home/ObtenerTipoMateriales",
            timeout: timing.ajax2min,
            data: {},
            success: function (result) {
                //for (var i = 0; i < result.Plantas.length; i++) {}

                var func = function (opt, item) {
                    opt.text(item.Descripcion.split("||")[1]);
                    opt.val(item.Codigo);
                    return opt;
                };

                UtilJs.fillSelect2('#select_Material', result.TipoMateriales, func, { placeholder: 'SELECCIONE', allowClear: false }, null, false);
            }
        });
    },

    fillComboPlantas: function () {
        UtilJs.execAjaxTrace({
            url: SitePath + "Home/ObtenerPlantas",
            timeout: timing.ajax2min,
            data: { },
            success: function (result) {
                Home.plantas = {};
                for (var i = 0; i < result.Plantas.length; i++) {
                    Home.plantas[result.Plantas[i].Codigo.split("||")[1]] = result.Plantas[i].Descripcion.split("||")[1];
                }

                var func = function (opt, item) {
                    opt.text(item.Descripcion.split("||")[1]);
                    opt.val(item.Codigo);
                    return opt;
                };
                UtilJs.fillSelect2('#select_Planta', result.Plantas, func, { placeholder: 'SELECCIONE', allowClear: false }, null, false);
            }
        });
    },

    fillComboAnchos: function () {
        UtilJs.execAjaxTrace({
            url: SitePath + "Home/ObtenerAnchos",
            timeout: timing.ajax2min,
            data: { },
            success: function (result) {
                var func = function (opt, item) {
                    opt.text(item.Descripcion);
                    opt.val(item.Codigo);
                    return opt;
                };
                UtilJs.fillSelect2('#select_Ancho', result.Plantas, func, { placeholder: 'SELECCIONE', allowClear: false }, null, false);
            }
        });
    },

    fillComboGrados: function () {
        UtilJs.execAjaxTrace({
            url: SitePath + "Home/ObtenerGrados",
            timeout: timing.ajax2min,
            data: { texto: $("#select_Planta").val() },
            success: function (result) {
                console.log(result);
                var func = function (opt, item) {
                    opt.text(item.Descripcion);
                    opt.val(item.Codigo);
                    return opt;
                };
                UtilJs.fillSelect2('#select_Grado', result.Grados, func, { placeholder: 'SELECCIONE', allowClear: false }, null, false);
            }
        });
    },

    addEventosIniciales: function () {
        UtilJs.addCommonListener({
            event: "select2:select",
            selector: "#select_Planta",
            callback: function (event) {
                Home.fillComboAnchos();
            }
        });

        UtilJs.addCommonListener({
            event: "select2:select",
            selector: "#select_Ancho",
            callback: function (event) {
                Home.fillComboGrados();
            }
        });

        UtilJs.addCommonListener({
            selector: '#btnLimpiarLevel0',
            callback: function (event) {
                //todo event limpiar
            }
        });

        UtilJs.addCommonListener({
            selector: '#btnLimpiarLevel0',
            callback: function (event) {
                //todo event limpiar
            }
        });
        //UtilJs.addCommonListener({
        //    selector: '#MapaPrincipal',
        //    callback: function (event) {

        //        if($("#info-panel")[0].style.width == "350px")
        //        Home.mostrarOcultar(false);
        //
        //    }
        //});

        UtilJs.addCommonListener({
            selector: '#BtnBuscar',
            callback: function (event) {
                Home.Buscar();
            }
        });
    },

    
    Buscar: function () {
        $.ajax({
            url: SitePath + "Home/ObjetenerPuntosTrenes",
            data: {},
            success: function (result) {
                if (Home.plantas.length <= 0) {
                    Home.fillComboPlantas();
                }

                for (var i = 0; i < Home.markers.length; i++) {
                    Home.markers[i].setMap(null);
                }
                
                Home.markers = [];

                for (var i = 0; i < result.Data.Ubicaciones.length; i++) {
                    var info = result.Data.Ubicaciones[i];

                    var coord = {
                        lat: parseFloat(info.Coordenada.split(',')[0]),
                        lng: parseFloat(info.Coordenada.split(',')[1])
                    };
                    /*
                     * Coordenada: "25.4741386,-99.3522535"
                     * Gondolas: 40
                     * Planta: "1"
                     * Toneladas: "23,456"
                     * Transporte: "Tren1"
                     */

                    var marker = new google.maps.Marker({
                        position: coord,
                        map: map,
                        icon: "/Images/train.png"
                    });

                    var contentString = '<div class="info-marker">'
                        + ' <div class="info-bar"></div>'
                        + '  <div class="info-content">'
                        + ' <label>UBICACION</label>'
                        + ' <div><label>Tren:</label><span>' + info.Transporte + '</span></div>'
                        + ' <div><label>No. Gondolas:</label><span>' + info.Gondolas + '</span></div>'
                        + ' <div><label>Toneladas:</label><span>' + info.Toneladas + '</span></div>'
                        + ' <div><label>Plantas:</label><span>' + Home.plantas[info.Planta] + '</span></div>'
                        + ' </div>'
                        + ' </div>';

                    marker.info = info;
                    marker.infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    marker.addListener('click', function () {
                        this.infowindow.open(map, this);
                    });
                    
                    marker.addListener('dblclick', function () {
                        Home.mostrarOcultar(true);
                        $("#planta").text(Home.plantas[this.info.Planta].toUpperCase());
                        $("#packinglist").text("CHU BONISLAND OCT18");
                        $("#piezas").text("72,049");
                        $("#toneladas").text(this.info.Toneladas.toUpperCase());
                        $("#proveedor").text("TERNIUM BRASIL");
                        $("#tren").text(this.info.Transporte.toUpperCase());
                        $("#fechaSalida").text("08-01-2019");
                        $("#status").text("EN VIAJE");
                        $("#ubicacion").text("MONTERREY");

                    });

                    Home.markers.push(marker);
                }
            }
        });
    },

    mostrarOcultar: function (val) {

            if (!val) {
                $("#info-panel")[0].style.display = 'none';
                $("#info-panel")[0].style.width = '0px';
                $("#MapaPrincipal")[0].style.width = '100%';
            } else {
                $("#info-panel")[0].style.display = 'block';
                $("#info-panel")[0].style.width = '350px';
                $("#MapaPrincipal")[0].style.width = 'calc(100% - 350px)';
            }

        }
    }