﻿; (function ($) {
    $.fn.gridFiltroPlantas = function (method) {
        var defaults = {};

        var settings = {
        };

        var helpers = {
            //{ data: null, title: helpers.renderFirstCheckbox(), orderable: false, width: '25px', className: 'text-center', render: function (data, type, row, meta) { return helpers.renderCheckbox(); } }
            redraw: function (idTiles, data) {
                
                $("#containergridFiltroPlantasTable" + idTiles).DataTable({
                    paging: false, info: false, scrollY: '40vh', scrollX: true, language: tableLang, order: [], destroy: true, scrollCollapse: true,searching:false,
                    data: data,
                    columns: [
                        {
                            data: null, title: helpers.renderFirstCheckbox(), orderable: false, width: '25px', render: function (data, type, row, meta) {
                                return helpers.renderCheckbox(data);
                            }
                        },
                         ]
                });

                setTimeout(function () {
                    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
                }, 350);

            }, 
            renderCheckbox: function (data) {
                return '<div class="checkbox checkbox-primary" style="padding-left:32px;"><input class="tipoProcesoMaterialItem" type="checkbox"><label class="">' + data.Nombre +'</label></div>';
            },
            renderFirstCheckbox: function () {
                return '<div class="checkbox checkbox-primary"><input id="tipoProcesoMaterialAll" type="checkbox"><label class="">Todos</label></div>';
            },
            addCheckboxEvent: function () {
                UtilJs.addCommonListener({
                    selector: "#tipoProcesoMaterialAll",
                    callback: function (evt) {
                        var estado = $(this).prop('checked');
                        $('.tipoProcesoMaterialItem').prop('checked', estado);
                    }
                });

                UtilJs.addCommonListener({
                    selector: ".tipoProcesoMaterialItem",
                    callback: function (evt) {
                        var estado = $('.tipoProcesoMaterialItem:checked').length == $('.tipoProcesoMaterialItem').length;
                        $('#tipoProcesoMaterialAll').prop('checked', estado);
                    }
                });

            }
        }

        // public methods can be called as
        // $(selector).gridFiltroPlantas('methodName', arg1, arg2, ... argn)
        var methods = {

            // this the constructor method that gets called when
            // the object is created
            init: function (options) {

                // iterate through all the DOM elements we are
                // attaching the plugin to
                return this.each(function () {

                    settings = $.extend({}, defaults, options);
                    $(this).data("settings", settings);

                    $(this).data("data", settings.data);

                    // Toma el id del elemento como identificador principal
                    var idTiles = $(this).attr("id");

                    $(this).addClass("pg-content");

                    // Arma los elementos principales de la grilla
                    var containergridAtributo = "<table id='containergridFiltroPlantasTable" + idTiles + "' class='table' style='width:100%;' data-child='table' data-effect='bounceInLeft' >"
                        + "</table>";
                    $(this).html(containergridAtributo);
                });
            },

            redraw: function () {

                var idTiles = $(this).attr("id");

                var data = $("#" + idTiles).data("data");

                helpers.redraw(idTiles, data);
                helpers.addCheckboxEvent();
                // Anima la visualización
                $("#containergridFiltroPlantasTable" + idTiles).animatePanel();
                var func = function (opt, item) {
                    opt.text(item.Texto);
                    opt.val(item.id);
                    return opt;
                };
          
            },
        }

        // if a method as the given argument exists
        if (methods[method]) {

            // call the respective method
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));

            // if an object is given as method OR nothing is given as argument
        } else if (typeof method === "object" || !method) {

            // call the initialization method
            return methods.init.apply(this, arguments);

            // otherwise
        } else {
            // trigger an error
            $.error('Method "' + method + '" does not exist in gridFiltroPlantas plugin!');
        }
    }

})(jQuery);