﻿using Common.Models.Comunes;
using Common.Models.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 namespace UbicacionTrenes.Helper
{
    /// <summary>
    /// Método genérico que unifica todos los pasos a dar en el Controller y que permite concentrarse sólo en llamar a la función deseada
    /// </summary>
    /// <param name="classMethod">Nombre de clase [.] nombre de método de quien invoca</param>
    /// <param name="function">función a ejecutar</param>
    /// <returns></returns>
    internal class GenericMethods
    {
        /// <summary>
        /// Método genérico que unifica todos los pasos a dar en el Controller y que permite concentrarse sólo en llamar a la función deseada
        /// </summary>
        /// <param name="classMethod">Nombre de clase [.] nombre de método de quien invoca</param>
        /// <param name="function">función a ejecutar</param>
        /// <returns></returns>
        public static ResponseModel PerformFunction(string classMethod, Func<GenericModel> function)
        {
            var response = new ResponseModel();

            //  Log.ActivityTimerStart(Log.LOG_CONTROLLER, classMethod);
            try
            {
                var result = function();
                response = result.GetResponse(classMethod);
            }
            //catch (CredentialException)
            //{
            //    throw;
            //}
            catch (Exception ex)
            {
                //  Log.Error(Log.LOG_CONTROLLER, string.Format("Error en {0}", classMethod), ex);
                response.Code = 2002;
                response.ErrorMessage = string.Format("Error en {0}: {1}", classMethod, ex.Message);
                response.ErrorDetail = ex.ToString();
            }
            finally
            {
                //    Log.ActivityTimerEnd(Log.LOG_CONTROLLER);
            }
            //    response.Trace = trace.GetTrace();
            return response;
        }
    }
}