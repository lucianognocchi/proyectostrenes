﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Comunes
{
    public class WsMessageModel
    {
        public WsMessageModel() { }

        public string Message { get; set; }
        public string Code { get; set; }
        public string Details { get; set; }
        public string stack { get; set; }
        public bool HasError { get; }
    }
}
