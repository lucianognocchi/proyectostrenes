﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.Models.Comunes
{
    public class ResponseModel
    {
        public ResponseModel() { }
        public ResponseModel(object data, object userInfo) { }
        public ResponseModel(int code, string errorMessage, string errorDetail) { }

        public int Code { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDetail { get; set; }
        public object Data { get; set; }
        public object AuxData { get; set; }
        public object UserInfo { get; set; }
        public object Trace { get; set; }
    }
}