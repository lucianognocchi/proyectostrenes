﻿using Common.Models.Comunes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DataAccess
{
    public class Connection
    {
        public DataSet dataSet;
        internal static OleDbConnection oleDbConnection;
        public Connection()
        {

        }
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        }
    }
}
